import { Modal } from './components/modal'

import './styles/all.scss'
window.Modal = Modal

// Drag & Drop
const zone1 = document.querySelector('.zone-1')
const zone2 = document.querySelector('.zone-2')
const element1 = document.querySelector('#dropElement1')
const element2 = document.querySelector('#dropElement2')
const element3 = document.querySelector('#dropElement3')
const element4 = document.querySelector('#dropElement4')
const element5 = document.querySelector('#dropElement5')

zone1.ondragover = allowDrop
zone2.ondragover = allowDrop

function allowDrop (event) {
  event.preventDefault()
}

element1.ondragstart = drag
element2.ondragstart = drag
element3.ondragstart = drag
element4.ondragstart = drag
element5.ondragstart = drag

function drag (event) {
  event.dataTransfer.setData('id', event.target.id)
}

zone1.ondrop = drop
zone2.ondrop = drop

function drop (event) {
  const elementId = event.dataTransfer.getData('id')
  let currentZoneId = event.target.id

  if (currentZoneId == 'dropHere') {
    currentZoneId = 'zoneDrop'
  }

  const to = document.getElementById(currentZoneId)
  const element = document.getElementById(elementId)

  if (currentZoneId == 'dropHere' || currentZoneId == 'zoneDrop') {
    const dropHere = document.getElementById('dropHere')
    to.insertBefore(element, dropHere)
  }
  if (currentZoneId == 'zoneDrag') {
    to.append(element)
  }

  updateDragDropCounters()
}

function updateDragDropCounters () {
  const drop = document.getElementById('zoneDrop')
  const drag = document.getElementById('zoneDrag')
  const dragCounter = document.getElementById('dragCounter')
  const dropCounter = document.getElementById('dragCounter')
  const dragCount = drag.childElementCount - 1
  const dropCount = drop.childElementCount - 2

  dropCounter.innerText = '(' + dropCount + ')'
  dragCounter.innerText = '(' + dragCount + ')'
}

window.onload = function () {
  updateDragDropCounters()
}

// Slider
let offset = 0
const slider = document.querySelector('.slide')

document.querySelector('.next').addEventListener('click', function () {
  offset -= 512
  if (offset <= slider.childElementCount * -512) {
    offset = 0
  }

  slider.style.left = offset + 'px'
})

document.querySelector('.prev').addEventListener('click', function () {
  if (offset == 0) {
    offset = (slider.childElementCount - 1) * -512
  } else {
    offset += 512
  }

  slider.style.left = offset + 'px'
})

// Scroll
const anchors = document.querySelectorAll('a[href*="#"]')

for (const anchor of anchors) {
  anchor.addEventListener('click', function (e) {
    e.preventDefault()

    const blockID = anchor.getAttribute('href').substr(1)

    document.getElementById(blockID).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    })
  })
}
