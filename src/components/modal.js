export class Modal {
  static showModal () {
    const modal = document.getElementById('modal')
    modal.classList.add('visible-modal')

    return false
  }

  static hideModal () {
    const modal = document.getElementById('modal')
    modal.classList.remove('visible-modal')

    return false
  }
}
